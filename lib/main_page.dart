import 'package:column_widget_example/Profile_page.dart';
import 'package:column_widget_example/login_page.dart';
import 'package:column_widget_example/main.dart';
import 'package:flutter/material.dart';
import 'main_page.dart';
class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'หน้าหลัก',
        ),
        backgroundColor: Colors.amber,
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.grey,
              ),
              child: Text('Burapha University',style: TextStyle(fontSize: 30),),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
              ),
              title: const Text('Home'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return MainPage();
                })
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.manage_accounts,
              ),
              title: const Text('Profile'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return Proflie();
                })
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.logout,
              ),
              title: const Text('Logout'),
              onTap: () {
               // Navigator.push(context, MaterialPageRoute(builder: (context){
                //  return LoginPage();
               // })
               // );
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            const SizedBox(height: 10,),
            Text("ประกาศ",style: Theme.of(context).textTheme.headline4,),
            //const SizedBox(height: 50.0),
            const Divider(),
            SizedBox(
              width: double.infinity,
                child: Container(
                  child: Card(child :Text(  "17/02/2566 : ประกาศสำหรับนิสิตชั้นปีที่ 1 ปีการศึกษา 2566 ให้มารับบัตรนิสิตได้ที่ธนาคารกรุงไทย สาขา มหาวิทยาลัยบูรพา",textAlign: TextAlign.left,style: TextStyle(fontSize: 16,height: 1.4, )),color: Colors.amber[200],),
                )
            ),

            SizedBox(
                width: double.infinity,
                child: Container(
                  child: Card(child :Text("17/02/2566 : ประกาศสำหรับนิสิตเฉพาะคณะวิทยาการสารสนเทศ เปิดรับสมัครทุนนักเรียนดีเด่นจำนวน 200 ทุน และทุนช่วยเหลือค่าอุปกรณ์นิสิตจำนวน 300 ทุน สำหรับนิสิตที่สนใจ คลิ๊กที่นี่ เพื่อดูรายละเอียดเพิ่มเติม",textAlign: TextAlign.left,style: TextStyle(fontSize: 16,height: 1.4, ),),color: Colors.amber[200]),

                )
            ),

            //Text("17/02/2566 : ประกาศสำหรับนิสิตชั้นปีที่ 1 ปีการศึกษา 2566 ให้มารับบัตรนิสิตได้ที่ธนาคารกรุงไทย สาขา มหาวิทยาลัยบูรพา",textAlign: TextAlign.left,style: TextStyle(fontSize: 16,height: 1.4, )),
            //const FlutterLogo(size: 100.0),


            //const Expanded(flex: 8,child: const FlutterLogo(size: 100.0)),

            //const SizedBox(height: 100.0),
            const Spacer(flex:10),

            //const SizedBox(height: 30.0),
            const Spacer(flex:3),


            const Spacer(flex:20),
          ],

        ),

      ),
    );
  }
}
Widget buildHeader(BuildContext context) => Container(
  padding: EdgeInsets.only(
    top: MediaQuery.of(context).padding.top,
  ),
);

Widget buildMenuitem(BuildContext context) => Column(
  children: [
    ListTile(
      leading: const Icon(Icons.home),
      title: const Text('Home'),
      onTap: (){},
    )
  ],
);
