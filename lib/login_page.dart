import 'package:flutter/material.dart';
import 'main_page.dart';
class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            //const SizedBox(height: 50.0),
            const Spacer(flex:5),
            //const FlutterLogo(size: 100.0),
            //const Expanded(flex: 8,child: const FlutterLogo(size: 100.0)),
            Image.network("https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Buu-logo11.png/200px-Buu-logo11.png",fit: BoxFit.cover),
            //const SizedBox(height: 100.0),
            const Spacer(flex:10),
            const TextField(decoration: InputDecoration(labelText: 'Username')),
            const TextField(decoration: InputDecoration(labelText: 'Password')),
            //const SizedBox(height: 30.0),
            const Spacer(flex:3),

            OutlinedButton( style: OutlinedButton.styleFrom(
              fixedSize: Size(100, 50),
            ),
                onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context){
                  return MainPage();
              })
              );
            }, child: const Text('Login' ,style: TextStyle(fontSize: 20),)),
            const Spacer(flex:20),
            Text("ลืมรหัสผ่าน?",style: TextStyle(color: Colors.red),)
          ],

        ),
      ),
    );
  }
}
