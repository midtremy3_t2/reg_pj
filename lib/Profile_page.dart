import 'package:column_widget_example/login_page.dart';
import 'package:column_widget_example/main.dart';
import 'package:flutter/material.dart';
import 'main_page.dart';
class Proflie extends StatelessWidget {
  const Proflie({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'ประวัตินิสิต',
          ),
          backgroundColor: Colors.amber,
        ),
        drawer: Drawer(
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(

                  color: Colors.grey,
                ),
                child: Text('Burapha University',style: TextStyle(fontSize: 30)),

              ),
              ListTile(
                leading: Icon(
                  Icons.home,
                ),
                title: const Text('Home'),
                
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return MainPage();
                  })
                  );
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.manage_accounts,
                ),
                title: const Text('Profile'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.logout,
                ),
                title: const Text('Logout'),
                onTap: () {
                //  Navigator.push(context, MaterialPageRoute(builder: (context){
                    //return LoginPage();
                //  })
                 // );
                },
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(15),
            child: Column(
              children: [
                SizedBox(
                  width: 120,height: 120,
                  child: Image.network("https://www.khaosod.co.th/wpapp/uploads/2022/02/%E0%B8%9E%E0%B8%9B%E0%B8%8A%E0%B8%A3.%E0%B9%80%E0%B8%8A%E0%B8%B7%E0%B9%88%E0%B8%AD.jpg"),
                ),
                const SizedBox(height: 10,),
                Text("Pattraporn Noothong",style: Theme.of(context).textTheme.headline4,),
                Text("คณะวิทยาการสารสนเทศ สาขาวิทยาการคอมพิวเตอร์",style: Theme.of(context).textTheme.bodyText2,),
                Text("สถานะ : กำลังศึกษา ",style: Theme.of(context).textTheme.bodyText2,),
                const SizedBox(height: 20,),
                SizedBox(
                  width: 200,
                  child: ElevatedButton(
                    onPressed: (){},
                    child: const Text("EditProFile",style: TextStyle(color: Colors.grey),),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.black,
                      side:  BorderSide.none,shape: const StadiumBorder()),

                  ),
                ),
                const SizedBox(height: 30,),
                const Divider(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                   Text(
                       'EDUCATION',
                            style: TextStyle(fontSize: 24,fontWeight: FontWeight.bold),

                   ),const SizedBox(height: 16,),
                    Text(
                      'ID : 63160212'
                      , style: TextStyle(fontSize: 16,height: 1.4),
                    ),
                    Text(
                      'ID card number: 0000000000000'
                      , style: TextStyle(fontSize: 16,height: 1.4),
                    ),
                    Text(
                      'Name : Pattraporn Noothong'
                      , style: TextStyle(fontSize: 16,height: 1.4),
                    ),
                    Text(
                      'faculty : Faculty of Informatics'
                      , style: TextStyle(fontSize: 16,height: 1.4),
                    ),
                    Text(
                      'campus : Bangsaen'
                      , style: TextStyle(fontSize: 16,height: 1.4),
                    ),
                    Text(
                      'course: 2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ '
                      , style: TextStyle(fontSize: 16,height: 1.4),
                    ),

                    Text(
                      'Teacher advisor: ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม '
                      , style: TextStyle(fontSize: 16,height: 1.4),
                    ),
                    Text(
                      'วุฒิก่อนเข้ารับการศึกษา : ม.6 3.03 '
                      , style: TextStyle(fontSize: 16,height: 1.4),
                    ),
                    Text(
                      'จบการศึกษาจาก: วิสุทธรังษี จังหวัดกาญจนบุรี '
                      , style: TextStyle(fontSize: 16,height: 1.4),
                    ),
                    Text(
                      'วิธีรับเข้า: รับตรงทั่วประเทศ '
                      , style: TextStyle(fontSize: 16,height: 1.4),
                    ),




                    const Divider(),





                  ],
                )


              ],
            ),

          ),
        )
    );
  }
}
Widget buildHeader(BuildContext context) => Container(
  padding: EdgeInsets.only(
    top: MediaQuery.of(context).padding.top,
  ),
);

Widget buildMenuitem(BuildContext context) => Column(
  children: [
    ListTile(
      leading: const Icon(Icons.home),
      title: const Text('Home'),
      onTap: (){},
    )
  ],
);
