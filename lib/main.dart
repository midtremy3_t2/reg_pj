import 'package:column_widget_example/main_page.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'login_page.dart';

void main() {
  runApp(const mainn());
}

class mainn extends StatelessWidget {
  const mainn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'REG',
        theme: ThemeData(
          primarySwatch: Colors.amber,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('BURAPHA UNIVERSITY'),

          ),

          body: const SafeArea(

            child: LoginPage(),
          ),

        ),
      ),
    );
  }

}
